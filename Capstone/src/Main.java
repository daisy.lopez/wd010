import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        String name;
        TeamA teamA = new TeamA();
        /* Enter Team Name */
        Scanner inputName = new Scanner(System.in);
        System.out.print("Enter team name >> ");
        name = inputName.nextLine();
        // Set Team Name to TeamA.java
        // teamA.setTeamName(name);

        /* Enter Number of Members */
        Scanner scan = new Scanner(System.in);
        int num;
        System.out.print("Enter number of members for " + name + ": ");
        num = scan.nextInt();

        ArrayList<Inhuman> team1 = new ArrayList<Inhuman>();
        ArrayList<Inhuman> team2 = new ArrayList<Inhuman>();
        Scanner scanner = new Scanner(System.in);

        String input;

        /* TEAM A */
        do {
            System.out.println(
                    "Create an Inhuman for Team A"
                            + "\n1: Alpha Primitive"
                            + "\n2: Bird People"
                            + "\n3: Super Inhuman"
                            + "\n4: Exit"
            );

            input = scanner.nextLine();
            //String newInhuman;

            if (input.equals("1")) {
                System.out.println("Name your inhuman...");
                var namee = scanner.nextLine();
                AlphaPrimitive alphaPrim =new AlphaPrimitive(namee);
                team1.add(alphaPrim);
                System.out.println(alphaPrim.display());


            } else if (input.equals("2")) {
                System.out.println("Name your inhuman...");

                var namee = scanner.nextLine();
                BirdPeople birdPeople=new BirdPeople(namee);
                team1.add(birdPeople);
                    birdPeople.getHP();
                    birdPeople.getAP();
                    birdPeople.getDefense();
                System.out.println(birdPeople.display());
            } else if (input.equals("3")) {
                System.out.println("Name your inhuman...");

                var namee = scanner.nextLine();
                SuperInhuman superInhuman=new SuperInhuman(namee);
                team1.add(superInhuman);
                superInhuman.getHP();
                superInhuman.getAP();
                superInhuman.getDefense();
                System.out.println(superInhuman.display());
            }
        } while(!input.equals("4"));

        teamA.setTeamMember(team1);
        System.out.println(teamA.toString());
//
//        for(Inhuman i : team1)
//        {
//            System.out.println(i);
//
//        }

        /* TEAM B */
        do {
            System.out.println(
                    "Create an Inhuman for Team B"
                            + "\n1: Alpha Primitive"
                            + "\n2: Bird People"
                            + "\n3: Super Inhuman"
                            + "\n4: Exit"
            );

            input = scanner.nextLine();
            String newInhuman;

            if (input.equals("1")) {
                System.out.println("Name your inhuman...");
                var namee = scanner.nextLine();
                AlphaPrimitive alphaPrim =new AlphaPrimitive(namee);
                team2.add(alphaPrim);
                System.out.println(alphaPrim.display());


            } else if (input.equals("2")) {
                System.out.println("Name your inhuman...");

                var namee = scanner.nextLine();
                BirdPeople birdPeople=new BirdPeople(namee);
                team2.add(birdPeople);
                System.out.println(birdPeople.display());
            } else if (input.equals("3")) {
                System.out.println("Name your inhuman...");

                var namee = scanner.nextLine();
                SuperInhuman superInhuman=new SuperInhuman(namee);
                team2.add(superInhuman);
                System.out.println(superInhuman.display());
            }
        } while(!input.equals("4"));

        /* DISPLAY OUTPUT */
//        for(int i=0; i<1; i++) {
//            //System.out.println(inhuman.get(i));
//        }


//            String name;
//            TeamA teamA = new TeamA();
//            Scanner input = new Scanner(System.in);
//
//            System.out.print("Enter team name >> ");
//            name = input.nextLine();
//            teamA.setTeamName(name);
//            Scanner scan = new Scanner(System.in);
//            int num;
//            System.out.print("Enter number of members for " + name + ": ");
//            num = scan.nextInt();
////            TeamA teamA = new TeamA();
//            int x;
//            final int NUM_TEAM_MEMBERS = num;
////            Scanner input = new Scanner(System.in);
////
////            System.out.print("Enter team name >> ");
////            name = input.nextLine();
////            teamA.setTeamName(name);
//
//
//            for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
//            {
//                System.out.print("Enter team member's name >> ");
//                name = input.nextLine();
//                teamA.setMember(x, name);
//            }
//            System.out.println("\nMembers of " + teamA.getTeamName());
//            for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
//                System.out.print(teamA.getMember(x) + "\n");
//            System.out.println();

    } // End of public static void main


} // End of Main class
