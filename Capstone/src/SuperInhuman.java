import java.util.Random;

public class SuperInhuman extends Inhuman {
    private String name = "char";
    private int ap = getRandomNumberInRange(100, 200); // 100 to 200
    private int hp = getRandomNumberInRange(40, 60); // 40 to 60
    private int defense= getRandomNumberInRange(10, 20); // 10 to 20

    public SuperInhuman(String name) {
        this.name = name;
        this.setHP(hp);
        this.setAP(ap);
        this.setDefense(defense);

    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
//    public AlphaPrimitive (String name, int hp, int ap, int defense) {
//        super(name, hp, ap, defense);
//        this.name = name;
//        this.hp  = hp;
//        this.ap = ap;
//        this.defense = defense;
//    }

    // Setter of name
    public void setName(String name) {
        this.name = name;
    }
    // getter of name
    public String getName() {
        return name;
    }
    // Setter of hp
    public void setHP(int hp) {
        this.hp = hp;
    }
    // getter of hp
    public int getHP() {
        return hp;
    }

    // Setter of ap
    public void setAP(int ap) {
        this.ap = ap;
    }
    // getter of ap
    public int getAP() {
        return ap;
    }

    // Setter of defense
    public void setDefense(int defense) {
        this.defense = defense;
    }
    // getter of defense
    public int getDefense() {
        return defense;
    }


    public String display() {
        return "Super Inhuman: \n"
                + "\t\tName: " +  this.getName()
                + "\n\t\tHP: " + hp
                + "\n\t\tAP: " + ap
                + "\n\t\tDEF: " + defense;
    }

//    public static void main(String[] args) {
//
//    }

}



//    private int ap = 100;
//    private int hp = 100;
//    private int defense= 88;
//    private String name="Fire";



//    String name;
//    final int NUM_TEAMS = 4;
//    TeamA[] teama = new TeamA [NUM_TEAMS];
//    int x;
//    int y;
//
//    final int NUM_TEAM_MEMBERS = 4;
//    Scanner input = new Scanner (System.in);
//        for(y=0; y< NUM_TEAMS; ++y) {
//        teama[y] = new TeamA ();
//        System.out.println ("Enter team name >> ");
//        name = input.nextLine();
//        teama[y].setMember(x, name);
//        for(x = 0; x < NUM_TEAM_MEMBERS; ++x)
//        {System.out.print("Enter team member's name >> ");
//            name = input.nextLine();
//            teama.setTeamName(name);
//        }}
//        System.out.println ("\nMembers of team " + teama.getTeamName());
//        for(y = 0; y< NUM_TEAMS;++y)
//    {System.out.println("\nMember of team " + teama[y].getTeamName());
//        for(x = 0; x< NUM_TEAM_MEMBERS; ++x)
//            System.out.println();
//
//    }
//        System.out.print (teama.getMember(x) + " ");
//        System.out.println();
//
//
//


