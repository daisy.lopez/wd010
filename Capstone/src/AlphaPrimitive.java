import java.util.Random;

public class AlphaPrimitive extends Inhuman {
    private String name = "char";
    private int ap = getRandomNumberInRange(100, 200); // 100 to 200
    private int hp = getRandomNumberInRange(40, 60); // 40 to 60
    private int defense= getRandomNumberInRange(10, 20); // 10 to 20

    public AlphaPrimitive(String name) {
        this.name = name;
        this.setHP(hp);
        this.setAP(ap);
        this.setDefense(defense);

    }

    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
//    public AlphaPrimitive (String name, int hp, int ap, int defense) {
//        super(name, hp, ap, defense);
//        this.name = name;
//        this.hp  = hp;
//        this.ap = ap;
//        this.defense = defense;
//    }

    // Setter of name
    public void setName(String name) {
        this.name = name;
    }
    // getter of name
    public String getName() {
        return name;
    }
    // Setter of hp
    public void setHP(int hp) {
        this.hp = hp;
    }
    // getter of hp
    public int getHP() {
        return hp;
    }

    // Setter of ap
    public void setAP(int ap) {
        this.ap = ap;
    }
    // getter of ap
    public int getAP() {
        return ap;
    }

    // Setter of defense
    public void setDefense(int defense) {
        this.defense = defense;
    }
    // getter of defense
    public int getDefense() {
        return defense;
    }


    public String display() {
        return "Alpha Primitive: \n"
                + "\t\tName: " +  this.getName()
                + "\n\t\tHP: " + hp
                + "\n\t\tAP: " + ap
                + "\n\t\tDEF: " + defense;
    }

//    public static void main(String[] args) {
//
//    }
}
