import java.util.ArrayList;

public class TeamA {

    private ArrayList<Inhuman> teamMember;


    public void setTeamMember(ArrayList<Inhuman> team)
    {
        teamMember = team;
    }
    public ArrayList<Inhuman> getTeamMember()
    {
        return teamMember;
    }

    public String toString() {
        return "Team A: " + getTeamMember();
    }
}
