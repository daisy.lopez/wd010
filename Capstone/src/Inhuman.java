import java.util.ArrayList;

public class Inhuman {
    public String name;
    private int hp=0;
    private int ap =0;
    private int defense;


    // Setter of name
    public void setName(String name) {
        this.name = name;
    }
    // getter of name
    public String getName() {
        return name;
    }
    // Setter of hp
    public void setHP(int hp) {
        this.hp = hp;
    }
    // getter of hp
    public int getHP() {
        return hp;
    }

    // Setter of ap
    public void setAP(int ap) {
        this.ap = ap;
    }
    // getter of ap
    public int getAP() {
        return ap;
    }

    // Setter of defense
    public void setDefense(int defense) {
        this.defense = defense;
    }
    // getter of defense
    public int getDefense() {
        return defense;
    }


}
