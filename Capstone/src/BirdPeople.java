import java.util.Random;

public class BirdPeople extends Inhuman{

    private String name = "char";
    private int ap = getRandomNumberInRange(100, 200);
    private int hp = getRandomNumberInRange(20, 40);
    private int defense= getRandomNumberInRange(20, 30);

    public BirdPeople(String name) {
        this.name = name;
        this.setHP(hp);
        this.setAP(ap);
        this.setDefense(defense);

    }


    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }




    // Setter of name
    public void setName(String name) {
        this.name = name;
    }
    // getter of name
    public String getName() {
        return name;
    }
    // Setter of hp
    public void setHP(int hp) {
        this.hp = hp;
    }
    // getter of hp
    public int getHP() {
        return hp;
    }

    // Setter of ap
    public void setAP(int ap) {
        this.ap = ap;
    }
    // getter of ap
    public int getAP() {
        return ap;
    }

    // Setter of defense
    public void setDefense(int defense) {
        this.defense = defense;
    }
    // getter of defense
    public int getDefense() {
        return defense;
    }


    public String display() {
        return "Bird People: \n"
                + "\t\tName: " +  this.getName()
                + "\n\t\tHP: " + hp
                + "\n\t\tAP: " + ap
                + "\n\t\tDEF: " + defense;
    }

}
