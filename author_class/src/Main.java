public class Main {
    public static void main(String[] args) {
        Author author1 = new Author("Arthur Conan Doyle", "May 22, 1859 ", "Edinburgh, Scotland");
        Book book1 = new Book("Sherlock Holmes", "Arthur Conan Doyle", "Crime Fiction");

        book1.bookAuthor = author1;
        System.out.println(book1.toString());
    }
}
