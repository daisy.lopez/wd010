public class Author {
    public String name;
    public String dob;
    public String pob;
    public String penName;


    public Author(String arg1, String arg2, String arg3) {
        this.name = arg1;
        this.dob = arg2;
        this.pob = arg3;
    }

    public Author(String arg1, String arg2, String arg3, String arg4) {
        this.name = arg1;
        this.dob = arg2;
        this.pob = arg3;
        this.penName = arg4;
    }

    @Override
    public String toString() {
        return "author: \n"
                + "\t\tname: " + this.name
                + "\n\t\tdate of birth: " + this.dob
                + "\n\t\tplace of birth: " + this.pob
                + "\n\t\tpen name: " + this.penName ;
    }

}
