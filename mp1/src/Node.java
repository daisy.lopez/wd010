/* Class containing left and right child of current
   node and value*/
public class Node
{
     int value;
     Node left, right;

    public Node(int item)
    {
        value = item;
        left = right = null;
    }


    // Root of Binary Tree
    Node root;

    Node()
    {
        root = null;
    }


    // Call insertRec
    public void insert(int key) {
        root = insertRec(root, key);
    }

    /* A recursive function to insert a new key in BST */
    Node insertRec(Node root, int key) {

        /* If the tree is empty, return a new node */
        if (root == null) {
            root = new Node(key);
            return root;
        }

        /* Otherwise, recur down the tree */
        if (key < root.value)
            root.left = insertRec(root.left, key);
        else if (key > root.value)
            root.right = insertRec(root.right, key);

        /* return the (unchanged) node pointer */
        return root;
    }



    /* POST-ORDER */
    void postOrder(Node node)
    {
        if (node == null)
            return;

        // first recur on left subtree
        postOrder(node.left);

        // then recur on right subtree
        postOrder(node.right);

        // now deal with the node
        System.out.print(node.value + " ");
    }

    /* IN-ORDER*/
    void inOrder(Node node)
    {
        if (node == null)
            return;

        /* first recur on left child */
        inOrder(node.left);

        /* then print the data of node */
        System.out.print(node.value + " ");

        /* now recur on right child */
        inOrder(node.right);
    }

    /* PRE-ORDER */
    void preOrder(Node node)
    {
        if (node == null)
            return;

        /* first print data of node */
        System.out.print(node.value + " ");

        /* then recur on left subtree */
        preOrder(node.left);

        /* now recur on right subtree */
        preOrder(node.right);

    }


    void postOrder()  {     postOrder(root); }
    void inOrder()    {     inOrder(root);   }
    void preOrder()   {     preOrder(root);  }


    // Getter & setter for the value.
    public int getValue(){

        return value;
    }
    public void setValue(int value){

        value = value;
    }

    // Getters & setters for left & right nodes.
    public Node getLeft(){
        return left;
    }
    public Node getRight(){
        return right;
    }
    public void setLeft(Node n){
        left = n;
    }
    public void setRight(Node n){
        right = n;
    }


}

