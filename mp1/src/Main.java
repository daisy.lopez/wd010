
public class Main
    {
        // Driver method
        public static void main(String[] args)
        {

            Node rootNode = new Node();
//            rootNode.root = new Node(6);
//            rootNode.root.left = new Node(3);
//            rootNode.root.right = new Node(4);
//            rootNode.root.right.right = new Node(8);
//            rootNode.root.left.right = new Node(5);
//            rootNode.root.right.left = new Node (7);
//            rootNode.root.right.left.right = new Node (9);
            rootNode.insert(6);
            rootNode.insert(3);
            rootNode.insert(4);
            rootNode.insert(8);
            rootNode.insert(5);
            rootNode.insert(7);
            rootNode.insert(9);

            System.out.println("\nIn-Order traversal: ");
            rootNode.inOrder();

            System.out.println("\nPre-Order traversal: ");
            rootNode.preOrder();

            System.out.println("\nPost-Order traversal: ");
            rootNode.postOrder();

//            rootNode.getValue(); // return 6
//            rootNode.getLeft().getValue(); // return 3
//            rootNode.getLeft().getLeft().getValue(); // return null
//            rootNode.getLeft().getRight().getValue(); // return 4
//            rootNode.getRight().getValue(); // return 8
//            rootNode.getRight().getLeft().getValue(); // return 7
//            rootNode.getRight().getRight().getValue(); // return 9

        }
    }

