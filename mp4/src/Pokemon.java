public class Pokemon extends Battle{

    private int hp;
    private int ap ;
    private String name;


    public Pokemon(String name, int hp, int ap) {
        super(name, hp, ap);
        this.name=name;
        this.hp = hp;
        this.ap=ap;
    }

    // Setter of name
    public void setName(String Name) {
        this.name = name;
    }
    // getter of name
    public String getName() {
        return name;
    }
    // Setter of hp
    public void setHP(int hp) {
        this.hp = hp;
    }
    // getter of hp
    public int getHP() {
        return hp;
    }

    // Setter of ap
    public void setAP(int ap) {
        this.ap = ap;
    }
    // getter of ap
    public int getAP() {
        return ap;
    }


}
