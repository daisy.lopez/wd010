public class WaterType extends Battle {
    private String type = "Grass";
    private String weakness="Fire";

    public String getType() {
        return type;
    }
    public String getWeakness() {
        return weakness;
    }

    public WaterType (String name, int hp, int ap) {
        super(name, hp, ap);
    }
}
