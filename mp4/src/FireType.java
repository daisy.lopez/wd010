public class FireType extends Battle{
    private String type = "Fire";
    private String weakness="Water";

    public String getType() {
        return type;
    }
    public String getWeakness() {
        return weakness;
    }

    public FireType (String name, int hp, int ap) {
        super(name, hp, ap);
    }
}
