public class Main {
    public static void main(String[] args) {

        Circle c1 = new Circle(5);
        Rectangle r1 = new Rectangle(2.0, 3.0);
        Square s1 = new Square(5.0);
        c1.toString();
        r1.toString();
        s1.toString();

        /* OUTPUT
            Square:
                side: 5.0
        */
        /* s1.setSide(7.0); s1.toString() will output:
            Square:
                side: 7.0
        */

        Shape[] shapes = {(Shape)c1, (Shape)r1, (Shape)s1

        };

        Double totalPerimeter=0.0;
        Double totalArea=0.0;


        for (int i = 0; i < shapes.length; i++) {
            //solve for perimeter;
            double newPer = shapes[i].getPerimeter();

            //solve for area
            double newArea= shapes[i].getArea();

            System.out.println(shapes[i]);
            System.out.println("\tperimeter: " + newPer);
            totalPerimeter+= newPer;
            System.out.println("\tarea: " + newArea);
            totalArea+= newArea;
        }

        System.out.println("total perimeter: " + totalPerimeter);
        System.out.println("total area: " + totalArea);

    }
}