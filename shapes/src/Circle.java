public class Circle extends Shape {

    private double radius;
    final double pi = Math.PI;

    public Circle (double r)
    {

        radius = r;
    }

    @Override
    public double getPerimeter() {
        return (2 * pi * radius);
    }

    //Override the abstract method declared in shape
    public double getArea()
    {return Math.PI * radius * radius;}

    public double getRadius()
    {return radius;}

    public void setRadius(double newRadius)
    {radius = newRadius;}

    public String toString( )
    {
        //return "Circle: \n" + "radius: "+radius+super.toString();
        return "Circle:"
                + "\n\tradius: " + getRadius();
    }



}
