public class Square extends Rectangle { //sub class
    public Square() {}

    public Square(double s) {
        super(s, s);
    }

    public double getSide() {
        return getWidth();
    }

    public void setSide(double s) {
        setWidth(s);
        setLength(s);
    }

    @Override
    public void setWidth(double s) {
        super.setWidth(s);
        super.setLength(s);
    }

    @Override
    public void setLength(double s) {
        super.setWidth(s);
        super.setLength(s);
    }

    @Override
    public String toString() {
//        return "Square \n"
//                + "\tside=" + getSide();
        return "Square \n" +
                "\tside: " + getSide()
                ;
    }

}

