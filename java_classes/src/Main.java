public class Main {
    public static void main(String[] args) {
        Person p1 = new Person("Zuitt" , 3);
        Pet pet1 = new Pet("Mozart", "Dog", "Golden Retriever");

//        p1.setAge(4);
        p1.setMyPet(pet1);
        p1.setAge( p1.getAge() +5);

        System.out.println(p1);


//        System.out.println(p1.toString());
//        System.out.println(pet1.toString());
//
//        System.out.println(p1.sayGoodAfternoon());


//        Person p2 = new Person("Zuitt2" , 3);
//        Person p3 = new Person("Coding Bootcamp");
//        Person p4 = new Person(7);

//        p1.age++;
//        System.out.println(p2.name);
//        System.out.println(p1.age);
//        System.out.println(p3.name);
//        System.out.println(p4.age);
//        System.out.println(p1.sayGoodAfternoon());
//        System.out.println(p1.saySomething("Peter! "));

        Teacher xp = new Teacher("Experience", 34, 34,"Makati");
        System.out.println(xp.toString());

        Doctor house = new Doctor("Gregory House", 49, 19,"St. Luke's Hospital");
        System.out.println(house.toString());

        Doctor strange = new Doctor("Stephen Strange", 45, 10,"Marvel Universe");

        Professional p2 = (Professional)xp;
        Professional p3 = (Professional)house;
        Professional p4 = (Professional)strange;
        Professional[] professionals = { p2, p3, p4 }; // 'professionals' is an array name
        for(int i=0; i<professionals.length; i++) {
           // System.out.println(professionals[i].doYourJob()); // Outputs the jobs of p2, p3 and p4
            System.out.println(professionals[i].getName()); // Outputs the names of p2, p3 and p4
        }
//        System.out.println(professionals[0]); // This prints out index 0 of the professionals array

//        Teacher p3 = (Teacher)p2;
////        p1.teach();
//
//        p3.doYourJob();
 //       System.out.println(p2.toString());

        house.doYourJob();
        xp.doYourJob();

    } // end of public static
} // end of public class Main
