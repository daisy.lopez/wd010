public class Pet {
    public String name;
    public String type;
    public String breed;

    public Pet() {}
    public Pet(String arg1, String arg2, String arg3) {
        this.name = arg1;
        this.type = arg2;
        this.breed = arg3;
    }

    public String toString() {
        return "Pet: \n"
                + "\t\tname: " + this.name
                + "\n\t\ttype: " + this.type
                + "\n\t\tbreed: " + this.breed ;
    }
}
