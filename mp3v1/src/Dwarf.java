public class Dwarf extends Fellow{
    private String weaponType = "Axe";
    private String weaponName;

    public String getWeaponType()
    {
        return  weaponType;
    }

    public String getWeaponName() {
        return weaponName;
    }

    public Dwarf(String name, String weaponName) {
        super(name);
        this.weaponName = weaponName;
    }

    public String toString(){
        return  "Dwarf "
                + "\t[name: " + getName()
                + "\tweaponType: " + getWeaponType()
                + "\tweaponName: " + getWeaponName()+ "]";
    }
}
