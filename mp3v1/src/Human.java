public class Human extends Fellow{
    private String weaponType = "Sword";
    private String weaponName;

    public String getWeaponType()
    {
        return  weaponType;
    }

    public String getWeaponName() {
        return weaponName;
    }

    public Human(String name, String weaponName) {
        super(name);
        this.weaponName = weaponName;
    }

    public String toString(){
        return  "Human"
                + "\t[name: " + getName()
                + "\tweaponType: " + getWeaponType()
                + "\tweaponName: " + getWeaponName() + "]";
    }
}
