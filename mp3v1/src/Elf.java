public class Elf extends Fellow{
    private String weaponType = "Bow and Arrow";
    private String weaponName;

    public String getWeaponType()
    {
        return  weaponType;
    }

    public String getWeaponName() {
        return weaponName;
    }

    public Elf(String name, String weaponName) {
        super(name);
        this.weaponName = weaponName;
    }

    public String toString(){
        return  "Elf"
                + "\t\t[name: " + getName()
                + "\tweaponType: " + getWeaponType()
                + "\tweaponName: " + getWeaponName() + "]";
    }
}
