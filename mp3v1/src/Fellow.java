public class Fellow {

    private String name;


    public Fellow(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getWeaponType()
    {
        return "none";
    }

    public  String getWeaponName()
    {
        return "none";
    }

    public String toString(){
        return  "Fellow "
                + "\t[name: " + getName()
                + "\tweaponType: " + getWeaponType()
                + "\tweaponName: " + getWeaponName() + "]";
    }


}
