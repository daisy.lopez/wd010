import javax.tools.StandardLocation;

public class Main {
    public static void main(String[] args) {
        Fellow frodo = new Fellow ("Frodo");
        Elf legolas = new Elf("Legolas", "Mirkwood Bow");
        Human aragorn = new Human("Aragorn", "Anduril");
        Dwarf gimli = new Dwarf("Gimli", "Parshu");



        System.out.println(frodo.toString());
        System.out.println(legolas.toString());
        System.out.println(aragorn.toString());
        System.out.println(gimli.toString());
    }
}
