public class Main {
    public static void main (String[] args) {

        Castle c1 = new Castle ("Minas Tirith","33", "20");
        Castle c2= new Castle ("Minas Morgul","20", "33");
        Castle c3 = new Castle ("Isengard","27", "58");
        Kingdom k1 = new Kingdom(c1,c2,c3);
        System.out.println(k1.displayCastleInfo());

    }
}