import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<String> fruits = new ArrayList<String>();
        ArrayList<Integer> numbers = new ArrayList<Integer>();
        Scanner scanner = new Scanner(System.in);

        fruits.add("Apple");
        fruits.add("Orange");
        fruits.add("Mango");

        String input;
        do {
            System.out.println(
                    "add to"
                            + "\n1: fruits"
                            + "\n2: numbers"
                            + "\n3: exit"
            );

            input = scanner.nextLine();
            String newFruit;
            int newNum;
            if (input.equals("1")) {
                System.out.println("what fruit to add?");
                newFruit = scanner.nextLine();
                fruits.add(newFruit);
            } else if (input.equals("2")) {
                System.out.println("what number to add?");
                newNum = scanner.nextInt();
                numbers.add(newNum);
            }
        } while(!input.equals("3"));

        for(int i=0; i<fruits.size(); i++) {
            System.out.println(fruits.get(i));
        }

    } // end of static void main
} // end of public class main
