public  abstract class Point { // superclass

    private int x;
    private int y;

    public Point() {}
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    // Setter of x
    public void setPointX(int x) {
        this.x = x;
    }
    // getter of x
    public int getPointX() {
        return x;
    }

    // Setter of y
    public void setPointY(int y) {
        this.y = y;
    }

    // getter of y
    public int getPointY() {
        return y;
    }

    public String introduce() {

        return "Point";
    }

    public String toString() {
        return "2D Point: "
                +"\n\t1: " + this.x
                + "\n\t2: " + this.y;
    }
} // end of class point



