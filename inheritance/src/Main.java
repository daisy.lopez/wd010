/*
    COMPOSITION - object as attribute of another object
    INHERITANCE - class 'extends' class
    ENCAPSULATION - private attributes and methods, use accessors & mutator
    POLYMORPHISM - object can be 2 object types
                     (watch out for overloaded methods)
*/
public class Main {
    public static void main(String[] args) {
    Point p1 = new Point(1, 2);
    //Point3D p2= new Point3D(1, 2, 3);


    p1.setPointX(11);
    p1.setPointY(21);

    System.out.println(p1.toString());

    //System.out.println(p2.toString());
    Point3D p3= new Point3D(p1, 33);
    //System.out.println(p3.toString());

    Point p2 = (Point)p3;

    System.out.println(p1.introduce());
    System.out.println(p2.introduce());
    }
}
